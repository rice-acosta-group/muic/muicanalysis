# sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error

../bin/run zprime --comparison machine_comparison_bsm_zp_sb_to_b \
'{"name": "LHmuC", "input": "root_files/lhmuc_bsm_zp_sb_to_b.root", "sigma": 2.7443e-12, "sigma_error": 1.15e-12}' \
'{"name": "MuIC2", "input": "root_files/muic2_bsm_zp_sb_to_b.root", "sigma": 0.24559e-12, "sigma_error": 0.0857e-12}' \
'{"name": "MuIC", "input": "root_files/muic_bsm_zp_sb_to_b.root", "sigma": 0.042615e-12, "sigma_error": 0.000683e-12}' \
