# sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error

# MZP,SIGMA_Q2min_1,SIGMA_Q2min_1_ERR,SIGMA_Q2min_20k,SIGMA_Q2min_20k_ERR
nc_bj="3412600e-12    504e-12     163030e-12    27.1e-12    5393.2e-12    0.999e-12"
nc_cj="18793000e-12   3700e-12    763920e-12    141e-12     17933e-12     5.71e-12"
nc_lj="36425000e-12   9540e-12    1683000e-12   534e-12     69953e-12     27.8e-12"
zp200="0.021871e-12   2.71e-18    0.060899e-12  1.81e-17    0.038271e-12  9.53e-18"
zp350="0.02224e-12    2.67e-18    0.069535e-12  2.05e-17    0.074429e-12  1.97e-17"
zp500="0.022327e-12   2.83e-18    0.072045e-12  2.12e-17    0.096958e-12  2.62e-17"
int200="311.07e-12    0.0651e-12  113.33e-12    0.0246e-12  13.974e-12    0.00264e-12"
int350="312.84e-12    0.0657e-12  118.71e-12    0.0267e-12  18.288e-12    0.00345e-12"
int500="313.15e-12    0.0738e-12  120.17e-12    0.0257e-12  20.01e-12     0.00396e-12"

# NC
function study_background() {
  ../bin/run zprime --machine muic2 --individual muic2_nc_bj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_bj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$1"', "sigma_error": '"$2"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_bj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_bj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine muic2 --individual muic2_nc_cj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_cj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_cj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_cj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine muic2 --individual muic2_nc_lj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_lj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_lj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${15}"', "sigma_error": '"${16}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC2_mz_200GeV_delbs_0p1_nc_lj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${17}"', "sigma_error": '"${18}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
}

function study_signal() {
#  # SIGNAL
#  ../bin/run zprime --machine muic2 --individual "muic2_mz_$1GeV_$2_zp_sb_to_b" \
#  '{
#    "datasets": [
#      {"input": "../in/zprime/MuIC2_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 100, "max_q2": 1000},
#      {"input": "../in/zprime/MuIC2_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_1k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 1000, "max_q2": 10000},
#      {"input": "../in/zprime/MuIC2_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_10k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 10000, "max_q2": -1}
#    ]
#  }' &
#  ../bin/run zprime --machine muic2 --individual "muic2_mz_$1GeV_$2_nc_bj_intf" \
#  '{
#    "datasets": [
#      {"input": "../in/intf/MuIC2_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 100, "max_q2": 1000},
#      {"input": "../in/intf/MuIC2_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 1000, "max_q2": 10000},
#      {"input": "../in/intf/MuIC2_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 10000, "max_q2": -1}
#    ]
#  }' &

  # AGGREGATE
  ../bin/run zprime --machine muic2 --signal "muic2_mz_$1GeV_$2_signal" \
  '{
    "mzp": '"$1"',
    "zp_pscan": "../in/xsec/zprime_muic2_pscan_q2min_100_xsec.csv",
    "intf_pscan": "../in/xsec/nc_bj_intf_muic2_pscan_q2min_100_xsec.csv",
    "datasets": [
      {"name": "zprime", "input": "root_files/muic2_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b.root"},
      {"name": "interference", "input": "root_files/muic2_mz_'"$1"'GeV_'"$2"'_nc_bj_intf.root"},
      {"name": "nc-b-jet", "input": "root_files/muic2_nc_bj_mg5.root"},
      {"name": "nc-c-jet", "input": "root_files/muic2_nc_cj_mg5.root"},
      {"name": "nc-l-jet", "input": "root_files/muic2_nc_lj_mg5.root"}
    ]
  }' &
}

#study_background $nc_bj $nc_cj $nc_lj
study_signal 200 'delbs_0p1' $zp200 $int200
study_signal 350 'delbs_0p1' $zp350 $int350
study_signal 500 'delbs_0p1' $zp500 $int500
