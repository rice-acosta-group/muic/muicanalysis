from ROOT import kBlue
from ROOT import kCyan
from ROOT import kGreen
from ROOT import kMagenta
from ROOT import kOrange
from ROOT import kRed
from ROOT import kTeal

serie_styles = {
    # RATIO
    'reference': {'label': 'Reference', 'color': kGreen + 2, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    '30fbm1': {'label': '30 fb^{-1}', 'color': kRed, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    '300fbm1': {'label': '300 fb^{-1}', 'color': kBlue, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    '3000fbm1': {'label': '3000 fb^{-1}', 'color': kMagenta, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    '400fbm1': {'label': '400 fb^{-1}', 'color': kGreen + 2, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    '237fbm1': {'label': '237 fb^{-1}', 'color': kGreen + 2, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},

    # ZPRIME
    'signal': {'label': '(LQ - Intf.) * -1', 'color': kGreen + 2, 'line-style': 1, 'marker': 33,
               'marker-size': 1.0},
    'interference': {'label': 'Intf. * -1', 'color': kTeal + 5, 'line-style': 1, 'marker': 33,
                     'marker-size': 1.0},
    # ZPRIME MASS
    '200GeV': {'label': 'm_{Z\'} = 200 GeV', 'color': kCyan + 2, 'line-style': 1, 'marker': 21,
               'marker-size': 0.85},
    '350GeV': {'label': 'm_{Z\'} = 350 GeV', 'color': kOrange + 2, 'line-style': 1, 'marker': 21,
               'marker-size': 0.85},
    '500GeV': {'label': 'm_{Z\'} = 500 GeV', 'color': kMagenta, 'line-style': 1, 'marker': 21,
               'marker-size': 0.85},
    '1TeV': {'label': 'm_{Z\'} = 1 TeV', 'color': kBlue, 'line-style': 1, 'marker': 20, 'marker-size': 0.85},
    '3TeV': {'label': 'm_{Z\'} = 3 TeV', 'color': kRed, 'line-style': 1, 'marker': 22, 'marker-size': 0.85},
    '10TeV': {'label': 'm_{Z\'} = 10 TeV', 'color': kGreen + 4, 'line-style': 1, 'marker': 23,
              'marker-size': 0.85},

    # JETS
    'nc-b-jet': {'label': 'NC b-jet', 'color': kRed, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    'nc-c-jet': {'label': 'NC c-jet', 'color': kBlue, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},
    'nc-l-jet': {'label': 'NC l-jet', 'color': kMagenta, 'line-style': 1, 'marker': 33, 'marker-size': 1.0},

    # MACHINES
    'LHmuC': {'label': 'LHmuC', 'color': kMagenta, 'line-style': 1, 'marker': 20,
              'marker-size': 0.85},
    'MuIC2': {'label': 'MuIC2', 'color': kRed, 'line-style': 1, 'marker': 22,
              'marker-size': 0.85},
    'MuIC': {'label': 'MuIC', 'color': kBlue, 'line-style': 1, 'marker': 21,
             'marker-size': 0.85},
    'HERA': {'label': 'HERA', 'color': kGreen + 4, 'line-style': 1, 'marker': 23,
             'marker-size': 0.85},
}
