from ROOT import kBlue
from ROOT import kGreen
from ROOT import kMagenta
from ROOT import kRed

serie_styles = {
    # MACHINES
    'LHmuC': {'label': 'LHmuC', 'color': kMagenta, 'line-style': 1, 'marker': 20, 'marker-size': 2},
    'muic2': {'label': 'MuIC2', 'color': kBlue, 'line-style': 1, 'marker': 22, 'marker-size': 2},
    'muic': {'label': 'MuIC', 'color': kRed, 'line-style': 1, 'marker': 21, 'marker-size': 2},
    'muic_hera': {'label': 'HERA', 'color': kGreen + 4, 'line-style': 1, 'marker': 23, 'marker-size': 2},

    # BEAMS
    'nc_mu': {'label': '#mu^{-} p NC', 'color': kBlue, 'line-style': 1, 'marker': 22, 'marker-size': 2},
    'nc_anti_mu': {'label': '#mu^{+} p NC', 'color': kBlue - 7, 'line-style': 7, 'marker': 29,
                   'marker-size': 2},
    'cc_mu': {'label': '#mu^{-} p CC', 'color': kRed, 'line-style': 1, 'marker': 22, 'marker-size': 2},
    'cc_anti_mu': {'label': '#mu^{+} p CC', 'color': kRed - 7, 'line-style': 7, 'marker': 29, 'marker-size': 2},
}
