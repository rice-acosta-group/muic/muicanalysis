import json

from ROOT import TFile

from muic.commands.pythia.comparisons.comparisons import ComparisonAnalyzer


def configure_parser(parser):
    parser.add_argument('datasets', metavar='DATASETS',
                        default=[], type=str, nargs='+',
                        help='List of datasets')


def run(args):
    # Unpack args
    datasets = [json.load(dataset) for dataset in args.datasets]

    # Analyze
    analyzer = ComparisonAnalyzer()

    for dataset in datasets:
        # UNPACK DATASET
        simulation = dataset[0]
        root_file_path = dataset[1]

        # LOG
        print('Reading: %s' % root_file_path)

        # OPEN
        root_file = TFile.Open(root_file_path, 'READ')

        # PROCESS
        analyzer.process_entry((simulation, root_file))

        # CLOSE
        root_file.Close()

    analyzer.write()
