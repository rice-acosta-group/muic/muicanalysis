from ROOT import TCanvas
from ROOT import TF1
from ROOT import TLatex
from ROOT import TLegend
from ROOT import gStyle

from muic.commands.pythia.commons.styles import serie_styles
from muic.core.analyzers import AbstractAnalyzer
from muic.core.plotters.basic import *
from muic.pythia.commons.colliders import machines


class ComparisonAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.fitted_functions = dict()

        # PLOT GROUPS
        self.plot_group_rules = {
            # MACHINES
            'LHmuC': lambda simulation: 'LHmuC_' in simulation,
            'muic2': lambda simulation: 'muic2_' in simulation,
            'muic': lambda simulation: 'muic_' in simulation and 'muic_hera_' not in simulation,
            'muic_hera': lambda simulation: 'muic_hera_' in simulation,
            # BEAMS
            'nc_mu': lambda simulation: '_nc_mu' in simulation,
            'nc_anti_mu': lambda simulation: '_nc_anti_mu' in simulation,
            'cc_mu': lambda simulation: '_cc_mu' in simulation,
            'cc_anti_mu': lambda simulation: '_cc_anti_mu' in simulation,
        }

        # NOTE PLOT GROUPS STRUCTURE = PLOT GROUP NAME -> CLASSIFIER NAME -> SIMULATION NAME
        self.plot_groups = {
            'Q2_dcs': dict(),
            'Q2_high_dcs': dict()
        }

    def process_entry(self, data):
        # UNPACK
        simulation = data[0]
        root_file = data[1]

        # CLASSIFY
        for plot_name, groups in self.plot_groups.items():
            for rule_name, rule in self.plot_group_rules.items():
                # SHORT-CIRCUIT
                if not rule(simulation):
                    continue

                # GET GROUP
                group = groups.get(rule_name)

                if group is None:
                    group = dict()
                    groups[rule_name] = group

                # GET PLOT
                plot = getattr(root_file, plot_name)

                if plot is not None:
                    plot = plot.Clone(rule_name + '-' + plot_name)

                # ADD TO GROUP
                group[simulation] = plot

    def init_fitted_function(self, function_variant, function_name, plot, params):
        # FIND MIN AND MAX
        nxbins = plot.GetXaxis().GetNbins()

        min_x = None
        max_x = None
        max_y = None

        for i in range(1, nxbins + 1):
            content = plot.GetBinContent(i)
            x_low = plot.GetXaxis().GetBinLowEdge(i)
            width = plot.GetXaxis().GetBinWidth(i)
            x_up = x_low + width

            if content != 0:
                if max_x is None or max_x < x_up:
                    max_x = x_up

                if min_x is None or x_low < min_x:
                    min_x = x_low

                if max_y is None or max_y < content:
                    max_y = content

        min_x = max(200, min_x)

        # CREATE FUNCTION
        function = None

        if function_variant in ['nc_mu', 'nc_anti_mu']:
            function = TF1(function_name + '_func',
                           '[2] * pow(x, [3]) * (1 + 1 / pow(1 + [1] / x, 2) + 2 / (1 + [1] / x)) * pow(1 - x / [0], [4])',
                           min_x, max_x)

            Emu = params['Emu']
            Ep = params['Ep']

            function.FixParameter(0, 4 * Emu * Ep)  # MAX Q2
            function.FixParameter(1, 91.1876 ** 2)  # MASS OF Z SQUARED
            function.SetParameter(2, 1e6)
            function.SetParLimits(2, 1e5, 1e10)
            function.SetParameter(3, -2)
            function.SetParLimits(3, -3, -2)
            function.SetParameter(4, 0)
            function.SetParLimits(4, 0, 30)

            # FIT
            res = plot.Fit(function, '0 M S Q R')

            print('%s,%f,%f,%f' % (function_name, res.Parameter(2), res.Parameter(3), res.Parameter(4)))

        elif function_variant in ['cc_mu', 'cc_anti_mu']:
            function = TF1(function_name + '_func',
                           '[2] * pow(x, [3]) * (1 / pow(1 + [1] / x + pow([5], 4)/pow(x,2), 2)) * pow(1 - x / [0], [4])',
                           min_x, max_x)

            Emu = params['Emu']
            Ep = params['Ep']

            function.FixParameter(0, 4 * Emu * Ep)  # MAX Q2
            function.FixParameter(1, 80.379 ** 2)  # MASS OF W SQUARED
            function.SetParameter(2, 1)
            function.SetParLimits(2, 1, 1e12)
            function.SetParameter(3, -2)
            function.SetParLimits(3, -3, -2)
            function.SetParameter(4, 0)
            function.SetParLimits(4, 0, 30)
            function.SetParameter(5, 0)
            function.SetParLimits(5, 0, 100)

            # FIT
            res = plot.Fit(function, '0 M S Q R')

            print('%s,%f,%f,%f,%f' % (
                function_name, res.Parameter(2), res.Parameter(3), res.Parameter(4), res.Parameter(5)))

        # SAVE
        self.fitted_functions[function_name] = function

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)

        machine_order = ['LHmuC', 'muic2', 'muic', 'muic_hera']

        group_data = {
            # MACHINES
            'LHmuC': {'title': 'LHmuC'},
            'muic2': {'title': 'MuIC2'},
            'muic': {'title': 'MuIC'},
            'muic_hera': {'title': 'HERA'},
            # BEAMS
            'nc_mu': {'title': '#mu^{-} p NC'},
            'nc_anti_mu': {'title': '#mu^{+} p NC'},
            'cc_mu': {'title': '#mu^{-} p CC'},
            'cc_anti_mu': {'title': '#mu^{+} p CC'},
        }

        def find_plot(group, simulation_name_contains):
            for simulation_name, plot in group.items():
                if simulation_name_contains in simulation_name:
                    return (simulation_name, plot)

            return (None, None)

        # FIT DCS
        for plot_group_name in ['Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]

            for rule_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                group = plot_group.get(rule_name)

                if group is None:
                    continue

                for simulation_name, plot in group.items():
                    machine_name = simulation_name.replace('_' + rule_name, '')
                    machine_data = machines[machine_name]

                    self.init_fitted_function(rule_name, simulation_name + '_Q2_high_dcs', plot, machine_data)
                    # self.init_fitted_function(rule_name, simulation_name + '_Q2_dcs', plot, machine_data)

        print(self.fitted_functions.keys())

        # PARTIAL CROSSECTIONS
        simulation_ics_bin_rules = {
            'Total': lambda Q2: True,
            'Q2>3e4': lambda Q2: Q2 > 3e4,
            'Q2>1e5': lambda Q2: Q2 > 1e5,
            'Q2>3e5': lambda Q2: Q2 > 3e5,
            'Q2>1e6': lambda Q2: Q2 > 1e6,
            'Q2>3e6': lambda Q2: Q2 > 3e6,
            'Q2>1e7': lambda Q2: Q2 > 1e7,
        }

        line_header = 'Dataset'

        for rule in simulation_ics_bin_rules.keys():
            line_header += ',' + rule + ',' + rule + ' Error'

        print(line_header)

        for rule_name in machine_order:
            plot_group = self.plot_groups['Q2_dcs'].get(rule_name)

            if plot_group is None:
                continue

            simulation_ics_bins = dict()
            simulation_ics_error_bins = dict()

            for simulation, plot in plot_group.items():
                nxbins = plot.GetXaxis().GetNbins()

                ics_bins = dict()
                ics_error_bins = dict()

                for i in range(1, nxbins + 1):
                    x_low = plot.GetXaxis().GetBinLowEdge(i)
                    width = plot.GetXaxis().GetBinWidth(i)
                    dcs = plot.GetBinContent(i)
                    dcs_error = plot.GetBinError(i)

                    pcs = dcs * width
                    pcs_error = dcs_error * width

                    for bin_rule_name, rule in simulation_ics_bin_rules.items():
                        if not rule(x_low):
                            continue

                        ics = ics_bins.get(bin_rule_name, 0)
                        ics_bins[bin_rule_name] = ics + pcs

                        ics_error = ics_error_bins.get(bin_rule_name, 0)
                        ics_error_bins[bin_rule_name] = ics_error + pcs_error ** 2

                simulation_ics_bins[simulation] = ics_bins
                simulation_ics_error_bins[simulation] = ics_error_bins

            for simulation in simulation_ics_bins.keys():
                line_record = simulation

                ics_bins = simulation_ics_bins[simulation]
                ics_error_bins = simulation_ics_error_bins[simulation]

                for rule in simulation_ics_bin_rules.keys():
                    ics = ics_bins.get(rule, 0)
                    ics_error = math.sqrt(ics_error_bins.get(rule, 0))

                    if ics == 0:
                        line_record += ','
                    else:
                        line_record += ',' + '{:e}'.format(ics) + ',' + '{:e}'.format(ics_error)

                print(line_record)

        # BEAM COMPARISON PLOTS
        agg_plot_styles = {
            'Q2_dcs': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_high_dcs': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
        }

        for plot_group_name in ['Q2_dcs', 'Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]
            agg_plot_style = agg_plot_styles[plot_group_name]

            for rule_name in machine_order:
                group = plot_group.get(rule_name)
                gdata = group_data.get(rule_name)

                if group is None or gdata is None:
                    continue

                agg_plot_name = plot_group_name + '_' + rule_name

                title = gdata['title']

                plot_canvas = TCanvas(agg_plot_name, '', 1080, 1080)
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(1)
                plot_canvas.SetLogy(1)
                plot_canvas.cd()

                frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                              agg_plot_style['x_up'], agg_plot_style['y_up'],
                                              'Differential Cross Section (%s)' % title)
                frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
                frame.GetYaxis().SetTitle('d#sigma / d(Q^{2}) [pb/GeV^{2}]')
                frame.GetYaxis().SetTitleOffset(1.650)
                frame.GetXaxis().SetTitleOffset(1.350)

                text_label = TLatex()
                text_label.SetTextSize(0.035)
                text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
                text_label.Draw('SAME')

                legend_x0 = 0.200
                legend_y0 = 0.200

                legend_2_x0 = 0.650
                legend_2_y0 = 0.750

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.2, legend_y0 + 0.1)
                legend.SetMargin(0.5)
                legend.SetFillStyle(0)

                legend_2 = TLegend(legend_2_x0, legend_2_y0, legend_2_x0 + 0.2, legend_2_y0 + 0.1)
                legend_2.SetMargin(0.5)
                legend_2.SetFillStyle(0)

                for serie_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                    simulation_name, plot = find_plot(group, rule_name + '_' + serie_name)

                    if simulation_name is None:
                        continue

                    fitted_function = self.fitted_functions.get(simulation_name + '_' + plot_group_name)

                    style = serie_styles[serie_name]

                    plot.SetMarkerStyle(style['marker'])
                    plot.SetMarkerColor(style['color'])
                    plot.SetMarkerSize(style['marker-size'])

                    if fitted_function is not None:
                        fitted_function.SetLineColor(style['color'])
                        fitted_function.SetLineWidth(2)
                        fitted_function.SetLineStyle(style['line-style'])

                    if '_cc_' in simulation_name:
                        legend.AddEntry(plot, style['label'], 'P')
                    elif '_nc_' in simulation_name:
                        legend_2.AddEntry(plot, style['label'], 'P')

                    if fitted_function is not None:
                        fitted_function.Draw('SAME')

                    plot.Draw('SAME HIST P E X0')

                legend.Draw()
                legend_2.Draw()

                plot_canvas.SaveAs(agg_plot_name + '.png')

        # MACHINE COMPARISON PLOTS
        agg_plot_styles = {
            'Q2_dcs_nc_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_dcs_nc_anti_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_dcs_cc_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_dcs_cc_anti_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_nc_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_nc_anti_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_cc_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_cc_anti_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
        }

        for plot_group_name in ['Q2_dcs', 'Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]

            for rule_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                agg_plot_style = agg_plot_styles[plot_group_name + '_' + rule_name]

                group = plot_group.get(rule_name)
                gdata = group_data.get(rule_name)

                if group is None or gdata is None:
                    continue

                agg_plot_name = plot_group_name + '_' + rule_name

                title = gdata['title']

                plot_canvas = TCanvas(agg_plot_name, '', 1080, 1080)
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(1)
                plot_canvas.SetLogy(1)
                plot_canvas.cd()

                frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                              agg_plot_style['x_up'], agg_plot_style['y_up'],
                                              'Differential Cross Section (%s)' % title)
                frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
                frame.GetYaxis().SetTitle('d#sigma / d(Q^{2}) [pb/GeV^{2}]')
                frame.GetYaxis().SetTitleOffset(1.650)
                frame.GetXaxis().SetTitleOffset(1.350)

                text_label = TLatex()
                text_label.SetTextSize(0.035)
                text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
                text_label.Draw('SAME')

                legend_x0 = 0.200
                legend_y0 = 0.200

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.2, legend_y0 + 0.2)
                legend.SetMargin(0.5)
                legend.SetFillStyle(0)

                for serie_name in machine_order:
                    simulation_name, plot = find_plot(group, serie_name + '_' + rule_name)

                    if simulation_name is None:
                        continue

                    fitted_function = self.fitted_functions.get(simulation_name + '_' + plot_group_name)

                    style = serie_styles[serie_name]

                    plot.SetMarkerStyle(style['marker'])
                    plot.SetMarkerColor(style['color'])
                    plot.SetMarkerSize(style['marker-size'])

                    if fitted_function is not None:
                        fitted_function.SetLineColor(style['color'])
                        fitted_function.SetLineWidth(2)
                        fitted_function.SetLineStyle(style['line-style'])

                    legend.AddEntry(plot, style['label'], 'P')

                    if fitted_function is not None:
                        fitted_function.Draw('SAME')

                    plot.Draw('SAME HIST P E X0')

                legend.Draw()

                plot_canvas.SaveAs(agg_plot_name + '.png')
