import numpy as np

from muic.commands.pythia.commons.parameters import ParameterSet
from muic.core.analyzers import AbstractAnalyzer
from muic.core.commons.kinematics import calc_eta_from_theta_rad
from muic.core.plotters.basic import *


class KinematicsAnalyzer(AbstractAnalyzer):

    def __init__(self, machine, reco_method):
        super().__init__()

        # Init
        self.parameters = ParameterSet(machine)
        self.reco_method = reco_method

        # Correlation
        self.q2_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'Q2_%s_correlation' % reco_method, 'Correlation',
            'True Q^{2} [GeV^{2}]', 'Reco Q^{2} [GeV^{2}]',
            xbins=self.parameters.q2bins,
            ybins=self.parameters.q2bins,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logx=True, logy=True,
            logz=True)
        self.x_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'x_%s_correlation' % reco_method, 'Correlation',
            'True x', 'Reco x',
            xbins=self.parameters.xbins,
            ybins=self.parameters.xbins,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logx=True, logy=True,
            logz=True)
        self.y_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'y_%s_correlation' % reco_method, 'Correlation',
            'True y', 'Reco y',
            xbins=self.parameters.ybins_uniform,
            ybins=self.parameters.ybins_uniform,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logx=True, logy=True,
            logz=True)
        self.sx_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'sx_%s_correlation' % reco_method, 'Correlation',
            'True s #upoint x [GeV^{2}]', 'Reco s #upoint x [GeV^{2}]',
            xbins=self.parameters.sx_uniform,
            ybins=self.parameters.sx_uniform,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logx=True, logy=True,
            logz=True)
        self.eta_mu_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'eta_mu_%s_correlation' % reco_method, 'Correlation',
            'True #eta_{#mu}', 'Reco #eta_{#mu}',
            xbins=self.parameters.eta_uniform,
            ybins=self.parameters.eta_uniform,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logz=True)
        self.eta_had_corr_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'eta_had_%s_correlation' % reco_method, 'Correlation',
            'True #eta_{had}', 'Reco #eta_{had}',
            xbins=self.parameters.eta_uniform,
            ybins=self.parameters.eta_uniform,
            min_val=1e-5,
            density=False,
            normalize_by='column',
            logz=True)

        # DCS
        self.q2_dcs_ubin_plotter = self.checkout_plotter(
            DCSPlotter,
            'Q2_%s_dcs_ubin' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
            xbins=self.parameters.q2bins_uniform,
            logx=True, logy=True)
        self.x_dcs_ubin_plotter = self.checkout_plotter(
            DCSPlotter,
            'x_%s_dcs_ubin' % reco_method, 'Differential Cross Section',
            'x', 'd#sigma / dx [pb]',
            xbins=self.parameters.xbins_uniform,
            logx=True, logy=True)
        self.q2_high_dcs_plot = self.checkout_plotter(
            DCSPlotter,
            'Q2_%s_high_dcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
            xbins=self.parameters.q2bins_high,
            logx=True, logy=True)
        self.q2_dcs_plot = self.checkout_plotter(
            DCSPlotter,
            'Q2_%s_dcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
            xbins=self.parameters.q2bins,
            logx=True, logy=True)
        self.x_dcs_plot = self.checkout_plotter(
            DCSPlotter,
            'x_%s_dcs' % reco_method, 'Differential Cross Section',
            'x', 'd#sigma / dx [pb]',
            xbins=self.parameters.xbins,
            logx=True, logy=True)
        self.y_dcs_plot = self.checkout_plotter(
            DCSPlotter,
            'y_%s_dcs' % reco_method, 'Differential Cross Section',
            'y', 'd#sigma / dy [pb]',
            xbins=self.parameters.ybins_uniform,
            logx=False, logy=True)
        self.sx_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'sx_%s_dcs' % reco_method, 'Differential Cross Section',
            's #upoint x [GeV^{2}]', 'd#sigma / d(s #upoint x) [pb/GeV]',
            xbins=self.parameters.sx_uniform,
            logx=True, logy=True)
        self.eta_mu_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'eta_mu_%s_dcs' % reco_method, 'Differential Cross Section',
            '#eta_{#mu}', 'd#sigma / d#eta_{#mu} [pb]',
            xbins=self.parameters.eta_uniform,
            logx=False, logy=True)
        self.eta_had_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'eta_had_%s_dcs' % reco_method, 'Differential Cross Section',
            '#eta_{had}', 'd#sigma / d#eta_{had} [pb]',
            xbins=self.parameters.eta_uniform,
            logx=False, logy=True)

        self.q2_x_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_x_%s_ddcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', 'x',
            'd^{2}#sigma / d(Q^{2})dx [pb/GeV^{2}]',
            xbins=self.parameters.q2bins,
            ybins=self.parameters.xbins,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=True, logz=True)
        self.q2_y_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_y_%s_ddcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', 'y',
            'd^{2}#sigma / d(Q^{2})dy [pb/GeV^{2}]',
            xbins=self.parameters.q2bins,
            ybins=self.parameters.ybins_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)
        self.q2_eta_mu_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_eta_mu_%s_ddcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', '#eta_{mu}',
            'd^{2}#sigma / d(Q^{2})d#eta_{mu} [pb/GeV^{2}]',
            xbins=self.parameters.q2bins,
            ybins=self.parameters.eta_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)
        self.q2_eta_had_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_eta_had_%s_ddcs' % reco_method, 'Differential Cross Section',
            'Q^{2} [GeV^{2}]', '#eta_{had}',
            'd^{2}#sigma / d(Q^{2})d#eta_{had} [pb/GeV^{2}]',
            xbins=self.parameters.q2bins,
            ybins=self.parameters.eta_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)

        # Resolutions
        self.q2_q2_vs_x_res2d_plot = self.checkout_plotter(
            RMS2DPlotter,
            'Q2_%s_Q2_vs_x_res2d' % reco_method, '#Delta Q^{2} / Q^{2} Muon Only',
            'x', 'Q^{2} [GeV^{2}]',
            xbins=self.parameters.xbins, ybins=self.parameters.q2bins)
        self.x_q2_vs_x_res2d_plot = self.checkout_plotter(
            RMS2DPlotter,
            'x_%s_Q2_vs_x_res2d' % reco_method, '#Delta x / x Muon Only',
            'x', 'Q^{2} [GeV^{2}]',
            xbins=self.parameters.xbins, ybins=self.parameters.q2bins)
        self.y_q2_vs_x_res2d_plot = self.checkout_plotter(
            RMS2DPlotter,
            'y_%s_Q2_vs_x_res2d' % reco_method, '#Delta y / y Muon Only',
            'x', 'Q^{2} [GeV^{2}]',
            xbins=self.parameters.xbins, ybins=self.parameters.q2bins)

    def process_entry(self, event):
        # Unpack data
        if self.reco_method == 'lep':
            sel_mu_eta = event.muon.smeared.eta
            sel_angljb = event.dis_reco_angljb
            sel_q2 = event.dis_reco_Q2_lep
            sel_x = event.dis_reco_x_lep
            sel_y = event.dis_reco_y_lep
        elif self.reco_method == 'jb':
            sel_mu_eta = event.muon.smeared.eta
            sel_angljb = event.dis_reco_angljb
            sel_q2 = event.dis_reco_Q2_jb
            sel_x = event.dis_reco_x_jb
            sel_y = event.dis_reco_y_jb
        elif self.reco_method == 'da':
            sel_mu_eta = event.muon.smeared.eta
            sel_angljb = event.dis_reco_angljb
            sel_q2 = event.dis_reco_Q2_da
            sel_x = event.dis_reco_x_da
            sel_y = event.dis_reco_y_da
        else:
            sel_mu_eta = event.mu_eta
            sel_angljb = event.rec_agljb
            sel_q2 = event.rec_Q2
            sel_x = event.rec_x
            sel_y = event.rec_y

        sel_had_eta = calc_eta_from_theta_rad(sel_angljb)
        sel_sx = sel_x * self.parameters.com_energy_squared

        true_had_eta = calc_eta_from_theta_rad(event.rec_agljb)
        true_sx = event.x * self.parameters.com_energy_squared

        # Correlation
        self.q2_corr_plotter.fill(event.rec_Q2, sel_q2, event.weighted_sigma * 1e-3)
        self.x_corr_plotter.fill(event.rec_x, sel_x, event.weighted_sigma * 1e-3)
        self.y_corr_plotter.fill(event.rec_y, sel_y, event.weighted_sigma * 1e-3)
        self.sx_corr_plotter.fill(true_sx, sel_sx, event.weighted_sigma * 1e-3)
        self.eta_mu_corr_plotter.fill(event.mu_eta, sel_mu_eta, event.weighted_sigma * 1e-3)
        self.eta_had_corr_plotter.fill(true_had_eta, sel_had_eta, event.weighted_sigma * 1e-3)

        # DCS
        self.q2_dcs_ubin_plotter.fill(
            sel_q2,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.x_dcs_ubin_plotter.fill(
            sel_x,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.q2_high_dcs_plot.fill(
            sel_q2,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.q2_dcs_plot.fill(
            sel_q2,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.x_dcs_plot.fill(
            sel_x,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.y_dcs_plot.fill(
            sel_y,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,
            event.norm_sigma_error * 1e-3)
        self.sx_dcs_plotter.fill(
            sel_sx,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb
        self.eta_mu_dcs_plotter.fill(
            sel_mu_eta,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb
        self.eta_had_dcs_plotter.fill(
            sel_had_eta,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb

        # DDCS
        self.q2_x_ddcs_plotter.fill(
            sel_q2,
            sel_x,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb
        self.q2_y_ddcs_plotter.fill(
            sel_q2,
            sel_y,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb
        self.q2_eta_mu_ddcs_plotter.fill(
            sel_q2,
            sel_mu_eta,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb
        self.q2_eta_had_ddcs_plotter.fill(
            sel_q2,
            sel_had_eta,
            event.evt_weight,
            event.input.id,
            event.norm_sigma * 1e-3,  # mb to pb
            event.norm_sigma_error * 1e-3)  # mb to pb

        # Resolutions
        q2_rerr = sel_q2 / event.rec_Q2 - 1
        x_rerr = sel_x / event.rec_x - 1
        y_rerr = sel_y / event.rec_y - 1

        if not np.isnan(q2_rerr):
            self.q2_q2_vs_x_res2d_plot.fill(
                event.rec_x, event.rec_Q2, q2_rerr,
                event.weighted_sigma * 1e-3)
        if not np.isnan(x_rerr):
            self.x_q2_vs_x_res2d_plot.fill(
                event.rec_x, event.rec_Q2, x_rerr,
                event.weighted_sigma * 1e-3)
        if not np.isnan(y_rerr):
            self.y_q2_vs_x_res2d_plot.fill(
                event.rec_x, event.rec_Q2, y_rerr,
                event.weighted_sigma * 1e-3)

    def merge(self, other):
        self.q2_corr_plotter.add(other.q2_corr_plotter)
        self.x_corr_plotter.add(other.x_corr_plotter)
        self.y_corr_plotter.add(other.y_corr_plotter)
        self.sx_corr_plotter.add(other.sx_corr_plotter)
        self.eta_mu_corr_plotter.add(other.eta_mu_corr_plotter)
        self.eta_had_corr_plotter.add(other.eta_had_corr_plotter)

        self.q2_dcs_ubin_plotter.add(other.q2_dcs_ubin_plotter)
        self.x_dcs_ubin_plotter.add(other.x_dcs_ubin_plotter)
        self.q2_high_dcs_plot.add(other.q2_high_dcs_plot)
        self.q2_dcs_plot.add(other.q2_dcs_plot)
        self.x_dcs_plot.add(other.x_dcs_plot)
        self.y_dcs_plot.add(other.y_dcs_plot)
        self.sx_dcs_plotter.add(other.sx_dcs_plotter)
        self.eta_mu_dcs_plotter.add(other.eta_mu_dcs_plotter)
        self.eta_had_dcs_plotter.add(other.eta_had_dcs_plotter)

        self.q2_x_ddcs_plotter.add(other.q2_x_ddcs_plotter)
        self.q2_y_ddcs_plotter.add(other.q2_y_ddcs_plotter)
        self.q2_eta_mu_ddcs_plotter.add(other.q2_eta_mu_ddcs_plotter)
        self.q2_eta_had_ddcs_plotter.add(other.q2_eta_had_ddcs_plotter)

        self.q2_q2_vs_x_res2d_plot.add(other.q2_q2_vs_x_res2d_plot)
        self.x_q2_vs_x_res2d_plot.add(other.x_q2_vs_x_res2d_plot)
        self.y_q2_vs_x_res2d_plot.add(other.y_q2_vs_x_res2d_plot)

    def post_production(self):
        pass
