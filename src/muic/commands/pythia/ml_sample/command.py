import json
import os

from muic.commands.pythia.ml_sample.signal import SignalAnalyzer
from muic.pythia.analysis.pythia_engine import mt_run
from muic.pythia.producers.DISReconstructionProducer import DISReconstructionProducer


def configure_parser(parser):
    parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                        type=str, default='ml_sample',
                        help='Output File Name')

    parser.add_argument('--nevents', dest='max_events', metavar='max_events',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--machine', dest='machine',
                        default='muic', type=str,
                        help='Specifies the machine being studied (MuIC, MuIC2, LHmuC)')

    parser.add_argument('datasets', metavar='DATASETS',
                        default=[], type=str, nargs='+',
                        help='List of datasets')


def run(args):
    # Unpack args
    filename = args.filename
    max_events = args.max_events
    machine = args.machine
    datasets = [json.loads(dataset) for dataset in args.datasets]

    # Normalize paths
    ndatasets = []

    for dataset in datasets:
        ndatasets.append((
            dataset['input'],
            dataset.get('sigma'), dataset.get('sigma_error'),
            dataset['min_q2'], dataset['max_q2']
        ))

    # Build Analysis
    main_analyzers = [
        SignalAnalyzer(filename, machine),
    ]

    def analysis_fn():
        return [
            DISReconstructionProducer(machine),
        ], [
            SignalAnalyzer(filename, machine),
        ]

    def merge_fn(outputs):
        for worker in outputs:
            for analyzer_id in range(len(worker)):
                analyzer = worker[analyzer_id]
                main_analyzers[analyzer_id].merge(analyzer)

    # Run Analysis
    mt_run(ndatasets, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    for main_analyzer in main_analyzers:
        main_analyzer.write()
