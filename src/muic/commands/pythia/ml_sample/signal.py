import numpy as np

from muic.commands.pythia.commons.parameters import ParameterSet
from muic.core.analyzers import AbstractAnalyzer
from muic.core.commons.kinematics import calc_eta_from_theta_rad


class SignalAnalyzer(AbstractAnalyzer):

    def __init__(self, filename, machine):
        super().__init__()

        # Init
        self.filename = filename
        self.parameters = ParameterSet(machine)

        self.event_count = 0
        self.total_sigma = 0

        self.list_weights = list()
        self.list_variables = list()
        self.list_parameters = list()

    def process_entry(self, event):
        # Unpack data
        meta = event.input.metadata

        # Event Weight
        evt_weight = event.weighted_sigma * 1e-3  # fb to pb

        self.event_count += 1
        self.total_sigma += evt_weight

        # Unpack Reco
        mu_e_reco = event.muon.smeared.energy
        mu_eta_reco = event.muon.smeared.p3.Eta()
        sum_e_reco = event.dis_reco_sum_e
        sum_px_reco = event.dis_reco_sum_px
        sum_py_reco = event.dis_reco_sum_py
        sum_pz_reco = event.dis_reco_sum_pz
        sum_e_minus_pz_reco = event.dis_reco_sum_e_minus_pz
        had_eta_reco = calc_eta_from_theta_rad(event.dis_reco_angljb)
        q2_lep = event.dis_reco_Q2_lep
        x_lep = event.dis_reco_x_lep
        y_lep = event.dis_reco_y_lep
        q2_jb = event.dis_reco_Q2_jb
        x_jb = event.dis_reco_x_jb
        y_jb = event.dis_reco_y_jb
        q2_da = event.dis_reco_Q2_da
        x_da = event.dis_reco_x_da
        y_da = event.dis_reco_y_da

        # Unpack Muon
        mu_e_true = event.muon.energy
        mu_eta_true = event.muon.eta

        had_eta_true = calc_eta_from_theta_rad(event.rec_agljb)
        q2_true = event.rec_Q2
        x_true = event.rec_x
        y_true = event.rec_y

        # Pack Parameters
        evt_parameters = np.asarray([
            mu_e_true, mu_eta_true,
            had_eta_true, q2_true, x_true, y_true])

        # Pack Variables
        evt_variables = np.asarray([
            mu_e_reco, mu_eta_reco,
            sum_e_reco, sum_px_reco, sum_py_reco, sum_pz_reco, sum_e_minus_pz_reco,
            had_eta_reco,
            q2_lep, x_lep, y_lep,
            q2_jb, x_jb, y_jb,
            q2_da, x_da, y_da
        ])

        self.list_weights.append(evt_weight)
        self.list_variables.append(evt_variables)
        self.list_parameters.append(evt_parameters)

    def merge(self, other):
        self.event_count += other.event_count
        self.total_sigma += other.total_sigma

        self.list_weights += other.list_weights
        self.list_variables += other.list_variables
        self.list_parameters += other.list_parameters

    def post_production(self):
        # Convert to numpy arrays
        weights = np.asarray(self.list_weights)
        variables = np.asarray(self.list_variables)
        parameters = np.asarray(self.list_parameters)

        # Normalize weights
        weights /= self.total_sigma

        # Shuffle
        random_index_array = np.arange(variables.shape[0])
        np.random.shuffle(random_index_array)

        weights = weights[random_index_array]
        variables = variables[random_index_array]
        parameters = parameters[random_index_array]

        # Split Datasets
        total_events = int(variables.shape[0])
        train_events = int(0.70 * total_events)
        test_events = total_events - train_events

        from_event = 0
        to_event = from_event + train_events
        weights_train = weights[from_event:to_event]
        variables_train = variables[from_event:to_event]
        parameters_train = parameters[from_event:to_event]

        from_event = to_event
        to_event = from_event + test_events
        weights_test = weights[from_event:to_event]
        variables_test = variables[from_event:to_event]
        parameters_test = parameters[from_event:to_event]

        # Log
        print('\n*********************************************************')
        print('Signal Sample Summary')
        print('*********************************************************')
        print('Event Count: %d' % self.event_count)
        print('Total Sigma [pb]: %d' % self.total_sigma)
        print('\nTotal Events: %d' % total_events)
        print('Train Events: %d' % train_events)
        print('Test Events: %d' % test_events)

        # Save
        np.savez_compressed(self.filename + '.npz',
                            weights_train=weights_train, variables_train=variables_train,
                            parameters_train=parameters_train,
                            weights_test=weights_test, variables_test=variables_test,
                            parameters_test=parameters_test)
