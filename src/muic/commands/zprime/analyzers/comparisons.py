import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TGraphErrors
from ROOT import TH1D
from ROOT import TLegend
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kFullDotLarge

from muic.commands.zprime.styles import serie_styles
from muic.core.analyzers import AbstractAnalyzer
from muic.core.plotters.basic import *
from muic.core.root.labels import draw_fancy_label


class ComparisonAnalyzer(AbstractAnalyzer):

    def __init__(self, parameters):
        super().__init__()

        self.parameters = parameters

        self.datasets = set()

        self.agg_plot_members = {
            'Q2_dcs': dict(),
            'Q2_dcs_ubin': dict(),
            'x_dcs': dict(),
            'x_dcs_ubin': dict(),
            'y_dcs': dict(),
            'eta_mu_dcs': dict(),
            'eta_had_dcs': dict()
        }

        self.agg_plot_styles = {
            'Q2_dcs': {'x_low': parameters.min_q2, 'x_up': parameters.max_q2, 'y_low': 1e-18, 'y_up': 1e4, 'logx': 1,
                       'logy': 1},
            'x_dcs': {'x_low': parameters.min_x, 'x_up': parameters.max_x, 'y_low': 1e-12, 'y_up': 1e12, 'logx': 1,
                      'logy': 1},
            'y_dcs': {'x_low': parameters.min_y, 'x_up': parameters.max_y, 'y_low': 1e-12, 'y_up': 1e8, 'logx': 0,
                      'logy': 1},
            'eta_mu_dcs': {'x_low': -10, 'x_up': 4, 'y_low': 1e-18, 'y_up': 1e12, 'logx': 0, 'logy': 1},
            'eta_had_dcs': {'x_low': -10, 'x_up': 4, 'y_low': 1e-18, 'y_up': 1e12, 'logx': 0, 'logy': 1},
            'Q2_dcs_ratios': {'x_low': parameters.min_q2, 'x_up': parameters.max_q2, 'y_low': 1e-6, 'y_up': 1e6,
                              'logx': 1,
                              'logy': 1},
        }

        self.valid_comparison_studies = {
            'machine_comparison': {
                'filename': 'dcs_machine_comparison',
                'datasets': ['LHmuC', 'MuIC2', 'MuIC']
            },
            'lumi_comparison': {
                'filename': 'dcs_lumi_comparison',
                'datasets': ['reference', '30fbm1', '300fbm1', '3000fbm1']
            },
        }

        self.valid_ratio_studies = {
            'lumi_ratio': {
                'filename': 'lumi_ratio',
                'datasets': ['reference',
                             '30fbm1',
                             '300fbm1',
                             '3000fbm1'],
                'comparisons': {
                    '30fbm1': {
                        'compare': '30fbm1',
                        'reference': 'reference',
                    },
                    '300fbm1': {
                        'compare': '300fbm1',
                        'reference': 'reference',
                    },
                    '3000fbm1': {
                        'compare': '3000fbm1',
                        'reference': 'reference',
                    },
                }
            },
            'mg5_vs_pythia': {
                'filename': 'mg5_vs_pythia',
                'datasets': ['nc-b-jet-mg5', 'nc-c-jet-mg5', 'nc-l-jet-mg5',
                             'nc-b-jet-pythia', 'nc-c-jet-pythia', 'nc-l-jet-pythia'],
                'comparisons': {
                    'nc-b-jet': {
                        'compare': 'nc-b-jet-mg5',
                        'reference': 'nc-b-jet-pythia',
                    },
                    'nc-c-jet': {
                        'compare': 'nc-c-jet-mg5',
                        'reference': 'nc-c-jet-pythia',
                    },
                    'nc-l-jet': {
                        'compare': 'nc-l-jet-mg5',
                        'reference': 'nc-l-jet-pythia',
                    }
                }
            },
        }

    def process_entry(self, data):
        # UNPACK
        name = data[0]
        root_file = data[1]

        # REGISTER
        self.datasets.add(name)

        # CLASSIFY
        for plot_name, members in self.agg_plot_members.items():
            # GET PLOT
            plot = getattr(root_file, plot_name)

            if plot is not None:
                plot = plot.Clone(name + '-' + plot_name)

            # ADD TO GROUP
            members[name] = plot

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)

        # VAR COMPARISON
        matched_study = None

        for study_name, study in self.valid_comparison_studies.items():
            if all([dataset in self.datasets for dataset in study['datasets']]):
                matched_study = study
                break

        if matched_study is not None:
            var_plots = [
                ('Q2', 'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
                 self.parameters.q2bins, 'Q2_dcs'),
                ('x', 'x', 'd#sigma / dx [pb]',
                 self.parameters.xbins, 'x_dcs'),
                ('y', 'y', 'd#sigma / dy [pb]',
                 self.parameters.ybins_uniform, 'y_dcs'),
                ('eta_mu', '#eta_{mu}', 'd#sigma / d#eta_{mu} [pb]',
                 self.parameters.eta_uniform, 'eta_mu_dcs'),
                ('eta_had', '#eta_{had}', 'd#sigma / d#eta_{had} [pb]',
                 self.parameters.eta_uniform, 'eta_had_dcs'),
            ]

            for var_plot in var_plots:
                var_name = var_plot[0]
                x_title = var_plot[1]
                y_title = var_plot[2]
                plot_bins = var_plot[3]
                plot_group = var_plot[4]

                agg_plot_members = self.agg_plot_members[plot_group]
                agg_plot_style = self.agg_plot_styles[plot_group]

                plot_canvas = TCanvas(var_name + '_' + matched_study['filename'], '')
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(agg_plot_style['logx'])
                plot_canvas.SetLogy(agg_plot_style['logy'])
                plot_canvas.cd()

                frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                              agg_plot_style['x_up'], agg_plot_style['y_up'],
                                              self.parameters.machine['label'])
                frame.GetXaxis().SetTitle(x_title)
                frame.GetXaxis().SetTitleOffset(1.350)
                frame.GetYaxis().SetTitle(y_title)
                frame.GetYaxis().SetTitleOffset(1.450)

                legend_x0 = 0.235
                legend_y0 = 0.200

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.250, legend_y0 + 0.200)
                legend.SetMargin(0.250)
                legend.SetFillStyle(0)

                plot_copies = []

                for dataset_name, series in agg_plot_members.items():
                    style = serie_styles.get(dataset_name)

                    if style is None:
                        continue

                    series = series.Clone(dataset_name + '_temp')
                    plot_copies.append(series)

                    if dataset_name == 'zprime':
                        series.Scale(self.parameters.b_as_b_prob)
                    elif dataset_name == 'nc-b-jet':
                        series.Scale(self.parameters.b_as_b_prob)
                    elif dataset_name == 'nc-c-jet':
                        series.Scale(self.parameters.c_as_b_prob)
                    elif dataset_name == 'nc-l-jet':
                        series.Scale(self.parameters.l_as_b_prob)

                    series.SetMarkerStyle(style['marker'])
                    series.SetMarkerColor(style['color'])
                    series.SetMarkerSize(style['marker-size'])

                    legend.AddEntry(series, style['label'], 'P')

                    series.Draw('SAME HIST P E X0')

                # TOTAL PLOT
                total_plot = TH1D('total_1d', 'Total', len(plot_bins) - 1, plot_bins)

                for bin_id in range(total_plot.GetNcells()):
                    total_val = 0
                    total_err2 = 0

                    for dataset_name, series in agg_plot_members.items():
                        val = series.GetBinContent(bin_id)
                        error = series.GetBinError(bin_id)

                        total_val += val
                        total_err2 += error ** 2

                    total_plot.SetBinContent(bin_id, total_val)
                    total_plot.SetBinError(bin_id, math.sqrt(total_err2))

                total_plot.SetMarkerStyle(kFullDotLarge)
                total_plot.SetMarkerColor(kBlack)
                total_plot.SetMarkerSize(0.5)

                total_plot.Draw('SAME HIST P E X0')

                legend.AddEntry(total_plot, 'Total', 'P')

                frame.Draw('SAME AXIS')
                frame.Draw('SAME AXIG')

                legend.Draw()

                draw_fancy_label(0.235, 0.800)

                plot_canvas.SaveAs('%s_%s.png' % (var_name, matched_study['filename']))

        # RATIOS
        matched_study = None

        for study_name, study in self.valid_ratio_studies.items():
            if all([dataset in self.datasets for dataset in study['datasets']]):
                matched_study = study
                break

        if matched_study is not None:
            agg_plot_members = self.agg_plot_members['Q2_dcs']

            # COMPARISON BUFFERS
            comparison_buffers = dict()

            for key, value in matched_study['comparisons'].items():
                comparison_buffers[key] = {
                    'flag': 0,
                    'compare': value['compare'],
                    'reference': value['reference'],
                    'compare_value': None,
                    'reference_value': None,
                    'compare_error': None,
                    'reference_error': None
                }

            # BUILD SERIES
            series_map = dict()

            for bin_id in range(self.parameters.q2bins + 2):  # Include Underflow and Overflow
                # RESET CACHE
                for comparison_name, comparison_buffer in comparison_buffers.items():
                    comparison_buffer['flag'] = 0
                    comparison_buffer['compare_value'] = None
                    comparison_buffer['reference_value'] = None
                    comparison_buffer['compare_error'] = None
                    comparison_buffer['reference_error'] = None

                # BUFFER VALUES
                for dataset_name, dataset_plot in agg_plot_members.items():
                    val = dataset_plot.GetXaxis().GetBinLowEdge(bin_id)
                    dcs = dataset_plot.GetBinContent(bin_id)
                    error = dataset_plot.GetBinError(bin_id)

                    for comparison_name, comparison_buffer in comparison_buffers.items():
                        comparison_value = comparison_buffer['compare']
                        reference_value = comparison_buffer['reference']

                        if comparison_value == dataset_name:
                            comparison_buffer['flag'] = comparison_buffer['flag'] | 1
                            comparison_buffer['compare_value'] = dcs
                            comparison_buffer['compare_error'] = error
                        elif reference_value == dataset_name:
                            comparison_buffer['flag'] = comparison_buffer['flag'] | 2
                            comparison_buffer['reference_value'] = dcs
                            comparison_buffer['reference_error'] = error

                        if comparison_buffer['flag'] == 3:
                            series = series_map.get(comparison_name)

                            if series is None:
                                series = {
                                    'x': list(),
                                    'y': list(),
                                    'y_err': list(),
                                }

                                series_map[comparison_name] = series

                            compare_value = comparison_buffer['compare_value']
                            compare_error = comparison_buffer['compare_error']
                            reference_value = comparison_buffer['reference_value']
                            reference_error = comparison_buffer['reference_error']

                            if reference_value == 0 or compare_value == 0:
                                continue

                            series['x'].append(val)
                            series['y'].append(compare_value / reference_value)

                            num = (reference_value * compare_error) ** 2 + (compare_value * reference_error) ** 2
                            denom = reference_value ** 4

                            series['y_err'].append(math.sqrt(num / denom))

            # PLOT
            agg_plot_style = self.agg_plot_styles['Q2_dcs_ratios']

            plot_canvas = TCanvas('', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.SetLogx(1 if agg_plot_style['logx'] else 0)
            plot_canvas.SetLogy(1 if agg_plot_style['logy'] else 0)
            plot_canvas.cd()

            frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                          agg_plot_style['x_up'], agg_plot_style['y_up'],
                                          '%s' % self.parameters.machine['label'])
            frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Ratio')
            frame.GetYaxis().SetTitleOffset(1.450)

            show_legend = len(series_map.keys()) > 1

            if show_legend:
                legend_x0 = 0.235
                legend_y0 = 0.235

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.250, legend_y0 + 0.200)
                legend.SetMargin(0.250)
                legend.SetFillStyle(0)

            temp_obj = list()

            def constant_rfunction(arr, par):
                return par[0]

            for series_name, series in series_map.items():
                style = serie_styles.get(series_name)

                if style is None:
                    continue

                plot = TGraphErrors(len(series['x']),
                                    array.array('d', series['x']),
                                    array.array('d', series['y']),
                                    np.zeros(len(series['x'])),
                                    array.array('d', series['y_err']))

                plot.SetMarkerStyle(style['marker'])
                plot.SetMarkerColor(style['color'])
                plot.SetMarkerSize(style['marker-size'])

                fit_function = TF1(
                    series_name + '_fit',
                    constant_rfunction,
                    agg_plot_style['x_low'], agg_plot_style['x_up'], 1)

                fit_result = plot.Fit(fit_function, '0 M S Q R')

                print(fit_result.Parameter(0))

                fit_function.SetLineColor(style['color'])
                fit_function.SetLineWidth(2)
                fit_function.Draw('SAME')

                plot.Draw('SAME P E')

                if show_legend:
                    legend.AddEntry(plot, style['label'], 'P L')

                temp_obj.append(plot)
                temp_obj.append(fit_function)
                temp_obj.append(fit_result)

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            if show_legend:
                legend.Draw()

            draw_fancy_label(0.235, 0.800)

            plot_canvas.SaveAs('%s.png' % matched_study['filename'])
