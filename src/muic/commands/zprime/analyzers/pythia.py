from ROOT import TCanvas
from ROOT import TLegend
from ROOT import kBlue
from ROOT import kRed

from muic.core.analyzers import AbstractAnalyzer
from muic.core.plotters.basic import *
from muic.core.root.labels import draw_fancy_label


class PythiaAnalyzer(AbstractAnalyzer):

    def __init__(self, parameters):
        super().__init__()

        self.parameters = parameters

        # HISTOS
        self.Q2_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'Q2_1d', parameters.machine['label'],
            'Q^{2}_{true} [GeV^{2}]', 'a.u.',
            xbins=parameters.q2bins_uniform,
            logx=True, logy=True)
        self.x_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'x_1d', parameters.machine['label'],
            'x_{true}', 'a.u.',
            xbins=parameters.xbins_uniform,
            logx=True, logy=True)
        self.y_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'y_1d', parameters.machine['label'],
            'y_{true}', 'a.u.',
            xbins=parameters.ybins_uniform,
            logx=True, logy=True)
        self.agljb_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'agljb_1d', parameters.machine['label'],
            '#gamma_{true} [rad]', 'a.u.',
            xbins=parameters.angljb_uniform,
            logx=False, logy=True)
        self.eta_had_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'eta_had_1d', parameters.machine['label'],
            '#eta_{had,true}', 'a.u.',
            xbins=parameters.eta_uniform,
            logx=False, logy=True)
        self.eta_mu_true_1d_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'eta_mu_1d', parameters.machine['label'],
            '#eta_{#mu,true}', 'a.u.',
            xbins=parameters.eta_uniform,
            logx=False, logy=True)

        # CROSS-SECTION
        # Sigma is in mb, so multiply by 1e9 to get pb
        self.Q2_dcs_ubin_plotter = self.checkout_plotter(
            DCSPlotter,
            'Q2_dcs_ubin', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
            xbins=parameters.q2bins_uniform,
            logx=True, logy=True)
        self.x_dcs_ubin_plotter = self.checkout_plotter(
            DCSPlotter,
            'x_dcs_ubin', parameters.machine['label'],
            'x', 'd#sigma / dx [pb]',
            xbins=parameters.xbins_uniform,
            logx=True, logy=True)
        self.Q2_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'Q2_dcs', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
            xbins=parameters.q2bins,
            logx=True, logy=True)
        self.x_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'x_dcs', parameters.machine['label'],
            'x', 'd#sigma / dx [pb]',
            xbins=parameters.xbins,
            logx=True, logy=True)
        self.y_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'y_dcs', parameters.machine['label'],
            'y', 'd#sigma / dy [pb]',
            xbins=parameters.ybins_uniform,
            logx=False, logy=True)
        self.eta_mu_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'eta_mu_dcs', parameters.machine['label'],
            '#eta_{#mu}', 'd#sigma / d#eta_{#mu} [pb]',
            xbins=parameters.eta_uniform,
            logx=False, logy=True)
        self.eta_had_dcs_plotter = self.checkout_plotter(
            DCSPlotter,
            'eta_had_dcs', parameters.machine['label'],
            '#eta_{had}', 'd#sigma / d#eta_{had} [pb]',
            xbins=parameters.eta_uniform,
            logx=False, logy=True)
        self.q2_x_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_x_ddcs', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', 'x',
            'd^{2}#sigma / d(Q^{2})dx [pb/GeV^{2}]',
            xbins=parameters.q2bins_uniform,
            ybins=parameters.xbins_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=True, logz=True)

        self.q2_y_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_y_ddcs', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', 'y',
            'd^{2}#sigma / d(Q^{2})dy [pb/GeV^{2}]',
            xbins=parameters.q2bins_uniform,
            ybins=parameters.ybins_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)

        self.q2_eta_mu_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_eta_mu_ddcs', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', '#eta_{mu}',
            'd^{2}#sigma / d(Q^{2})d#eta_{mu} [pb/GeV^{2}]',
            xbins=parameters.q2bins_uniform,
            ybins=parameters.eta_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)

        self.q2_eta_had_ddcs_plotter = self.checkout_plotter(
            DDCSPlotter,
            'Q2_eta_had_ddcs', parameters.machine['label'],
            'Q^{2} [GeV^{2}]', '#eta_{had}',
            'd^{2}#sigma / d(Q^{2})d#eta_{had} [pb/GeV^{2}]',
            xbins=parameters.q2bins_uniform,
            ybins=parameters.eta_uniform,
            z_low=1e-15, z_up=1e2,
            logx=True, logy=False, logz=True)

        self.weight_sums = dict()

    def process_entry(self, data):
        # UNPACK
        meta = data[0]
        event = data[1]

        # SAVE WEIGHT SUMS
        self.weight_sums[meta['__id__']] = meta['weight_sum']

        # DATA
        had_eta = -math.log(math.tan(event.rec_agljb / 2))

        # PLOT VARS
        self.Q2_true_1d_plotter.fill(event.rec_Q2, event.evt_weight)
        self.x_true_1d_plotter.fill(event.rec_x, event.evt_weight)
        self.y_true_1d_plotter.fill(event.rec_y, event.evt_weight)
        self.agljb_true_1d_plotter.fill(event.rec_agljb, event.evt_weight)
        self.eta_mu_true_1d_plotter.fill(event.mu_eta, event.evt_weight)
        self.eta_had_true_1d_plotter.fill(had_eta, event.evt_weight)

        # SIGMA
        # Sigma is in mb, so multiply by 1e9 to get pb
        self.Q2_dcs_ubin_plotter.fill(
            event.rec_Q2,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.x_dcs_ubin_plotter.fill(
            event.rec_x,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.Q2_dcs_plotter.fill(
            event.rec_Q2,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.x_dcs_plotter.fill(
            event.rec_x,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.y_dcs_plotter.fill(
            event.rec_y,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.eta_mu_dcs_plotter.fill(
            event.mu_eta,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.eta_had_dcs_plotter.fill(
            had_eta,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.q2_x_ddcs_plotter.fill(
            event.rec_Q2,
            event.rec_x,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.q2_y_ddcs_plotter.fill(
            event.rec_Q2,
            event.rec_y,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.q2_eta_mu_ddcs_plotter.fill(
            event.rec_Q2,
            event.mu_eta,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        self.q2_eta_had_ddcs_plotter.fill(
            event.rec_Q2,
            had_eta,
            event.evt_weight,
            meta['__id__'],
            meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
            meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

    def post_production(self):
        total_weight_sum = 0

        for weight_sum in self.weight_sums.values():
            total_weight_sum += weight_sum

        # COMPARE ETA
        muon_plot = self.eta_mu_true_1d_plotter.plot.Clone('eta_mu_true_copy')
        had_plot = self.eta_had_true_1d_plotter.plot.Clone('eta_had_true_copy')

        maximum = max(muon_plot.GetMaximum(), had_plot.GetMaximum())

        plot_canvas = TCanvas('eta_1d', '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.cd()

        frame = plot_canvas.DrawFrame(-10, 0, 4, maximum, self.parameters.machine['label'])
        frame.GetXaxis().SetTitle('#eta_{true}')
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetYaxis().SetTitle('a.u.')
        frame.GetYaxis().SetTitleOffset(1.450)
        frame.GetYaxis().SetMaxDigits(3)

        legend_x0 = 0.235
        legend_y0 = 0.400

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.250, legend_y0 + 0.200)
        legend.SetMargin(0.250)
        legend.SetFillStyle(0)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        muon_plot.SetLineColorAlpha(kBlue, 1.0)
        had_plot.SetLineColorAlpha(kRed, 1.0)
        muon_plot.SetFillColorAlpha(kBlue, 0.3)
        had_plot.SetFillColorAlpha(kRed, 0.3)

        legend.AddEntry(muon_plot, 'Scattered Muon', 'F')
        legend.AddEntry(had_plot, 'Reco Hadron', 'F')

        muon_plot.Draw('SAME HIST')
        had_plot.Draw('SAME HIST')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.235, 0.800)

        plot_canvas.SaveAs('eta_1d.png')
