import numpy as np


# Digitize
# https://github.com/jiafulow/user-notebooks/blob/L1MuonTrigger-P2_11_1_7/emtf_nbtools.py
def digitize_inclusive(x, bins):
    """
    Digitize according to how np.histogram() computes the histogram. All but the last
    (righthand-most) bin is half-open i.e. [a, b). The last bin is closed i.e. [a, b].
    Underflow values return an index of 0, overflow values return an index of len(bins)-2.
    Examples:
    --------
    >>> digitize_inclusive(0, [1,2,3,4])
    array(0)
    >>> digitize_inclusive(1, [1,2,3,4])
    array(0)
    >>> digitize_inclusive(1.1, [1,2,3,4])
    array(0)
    >>> digitize_inclusive(2, [1,2,3,4])
    array(1)
    >>> digitize_inclusive(3, [1,2,3,4])
    array(2)
    >>> digitize_inclusive(4, [1,2,3,4])
    array(2)
    >>> digitize_inclusive(4.1, [1,2,3,4])
    array(2)
    >>> digitize_inclusive(5, [1,2,3,4])
    array(2)
    """
    bin_edges = np.asarray(bins)
    n_bin_edges = bin_edges.size

    if n_bin_edges < 2:
        raise ValueError('`bins` must have size >= 2')

    if bin_edges.ndim != 1:
        raise ValueError('`bins` must be 1d')

    if np.any(bin_edges[:-1] > bin_edges[1:]):
        raise ValueError('`bins` must increase monotonically')

    x = np.asarray(x)
    x = x.ravel()

    # bin_index has range 0..len(bins)
    bin_index = bin_edges.searchsorted(x, side='right')

    # modified bin_index has range 0..len(bins)-2
    bin_index[bin_index == n_bin_edges] -= 1
    bin_index[bin_index != 0] -= 1
    bin_index = np.squeeze(bin_index)

    return int(bin_index)


# Intervals
def gen_fixed_width_bin_edges(begin, end, step):
    return np.arange(begin - step / 2, end + step * 3 / 2, step)


def gen_fixed_inv_width_bin_edges(begin, end, step, add_negatives=False):
    edges = gen_fixed_width_bin_edges(begin, end, step)

    # Invert
    edges = 1 / edges

    # Sort Edges and Remove Zero
    edges = np.sort(edges[edges != 0])

    # Combine
    if add_negatives:
        edges = np.asarray([*(edges[::-1] * -1.), *edges[:]])

    return edges
