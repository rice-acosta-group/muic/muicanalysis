import math

import numpy as np


def calc_mad(arr):
    arr = np.asarray(arr)
    median = np.median(arr)
    mad = np.median(np.abs(arr - median))
    sigma = mad * 1.4826

    return median, sigma


class Simple2DBin:

    def __init__(self, x_bins=None, y_bins=None):
        self.n = 0
        self.x = list()
        self.x_sum = 0
        self.x2_sum = 0
        self.y = list()
        self.y_sum = 0
        self.y2_sum = 0

        self.x_bins = x_bins
        self.y_bins = y_bins

    def add(self, other):
        self.n += other.n
        self.x += other.x
        self.x_sum += other.x_sum
        self.x2_sum += other.x2_sum
        self.y += other.y
        self.y_sum += other.y_sum
        self.y2_sum += other.y2_sum

    def append(self, x, y):
        self.n += 1
        self.x.append(x)
        self.x_sum += x
        self.x2_sum += (x ** 2)
        self.y.append(y)
        self.y_sum += y
        self.y2_sum += (y ** 2)

    def get_x_pos(self):
        mean_x = self.x_sum / self.n
        mean_x2 = self.x2_sum / self.n
        sigma_x = math.sqrt(mean_x2 - mean_x ** 2)

        return mean_x, sigma_x

    def get_y_pos(self):
        mean_y = self.y_sum / self.n
        mean_y2 = self.y2_sum / self.n
        sigma_y = math.sqrt(mean_y2 - mean_y ** 2)

        return mean_y, sigma_y
