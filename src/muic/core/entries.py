from abc import ABC, abstractmethod


class ValueIsMissing(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ValueIsMissing, cls).__new__(cls)
        return cls.instance


VALUE_IS_MISSING = ValueIsMissing()


class Entry(object):

    def __init__(self, tree, id, producers):
        self.tree = tree
        self.id = id
        self.producers = producers

        self.cache = dict()
        self.production_stack = set()
        self.branches_used = set()

        self.__initialized = True

    def __getattr__(self, field):
        # Try to get from cache
        value = self.get_from_cache(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to get from tree
        value = self.get_from_tree(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to produce value
        value = self.get_from_producer(field)

        if value is not VALUE_IS_MISSING:
            return value

        raise AttributeError(
            '[Entry] Field does not have a value, producer, nor does it exists in the tree: %s' % field)

    def __setattr__(self, key, value):
        # Allow setting attributes in __init__
        if '_Entry__initialized' not in self.__dict__:
            dict.__setattr__(self, key, value)
        # If it's an internal variable treat it as usual
        elif key in self.__dict__:
            dict.__setattr__(self, key, value)
        # Add it to the cache
        else:
            self.cache[key] = value

    def reset_field(self, key):
        if key not in self.cache:
            return

        self.cache.pop(key)

    def get_from_cache(self, field):
        return self.cache.get(field, VALUE_IS_MISSING)

    def get_from_producer(self, field):
        producer = self.producers.get(field, None)

        if producer is None:
            return VALUE_IS_MISSING

        if field in self.production_stack:
            raise RecursionError('[Entry] Field has a recursive dependency on itself: %s' % field)

        self.production_stack.add(field)
        products = producer.extract(self)
        self.cache.update(products)
        self.production_stack.remove(field)

        return products[field]

    def get_from_tree(self, field):
        branch = getattr(self.tree, field, VALUE_IS_MISSING)

        if branch is not VALUE_IS_MISSING:
            self.branches_used.add(field)

        return branch


class Producer(ABC):

    @abstractmethod
    def provides(self):
        raise NotImplementedError()

    @abstractmethod
    def extract(self, entry):
        raise NotImplementedError()

    def summary(self):
        pass


class SimpleProducer(Producer):

    def __init__(self, key, func):
        self.key = key
        self.func = func

    def provides(self):
        return [self.key]

    def extract(self, entry):
        return {self.key: self.func(entry)}
