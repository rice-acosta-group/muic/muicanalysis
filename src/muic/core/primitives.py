class ValueIsMissing(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ValueIsMissing, cls).__new__(cls)
        return cls.instance


VALUE_IS_MISSING = ValueIsMissing()


class Primitive(object):

    def __init__(self, entry, branch_prefix):
        self.entry = entry
        self.branch_prefix = branch_prefix
        self.branch_aliases = dict()
        self.producers = dict()

        self.cache = dict()
        self.production_stack = set()

        self.__initialized = True

    def __getattr__(self, field):
        # Try to get from cache
        value = self.get_from_cache(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to produce value
        value = self.get_from_producer(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to get from entry
        value = self.get_from_entry(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Error
        raise AttributeError(
            '[Primitive] Field does not have a value, producer, nor does it exists in the entry: %s' % field)

    def __setattr__(self, key, value):
        # Allow setting attributes in __init__
        if '_Primitive__initialized' not in self.__dict__:
            dict.__setattr__(self, key, value)
        # If it's an internal variable treat it as usual
        elif key in self.__dict__:
            dict.__setattr__(self, key, value)
        # Add it to the fields
        else:
            self.cache[key] = value

    def get_from_cache(self, field):
        return self.cache.get(field, VALUE_IS_MISSING)

    def get_from_producer(self, field):
        producer = self.producers.get(field, None)

        if producer is None:
            return VALUE_IS_MISSING

        if field in self.production_stack:
            raise RecursionError('[Primitive] Field has a recursive dependency on itself: %s' % field)

        self.production_stack.add(field)
        product = producer()
        self.cache[field] = product
        self.production_stack.remove(field)

        return product

    def get_from_entry(self, field):
        branch_name = self.branch_prefix + self.branch_aliases.get(field, field)
        return getattr(self.entry, branch_name, VALUE_IS_MISSING)


class VectorPrimitive(Primitive):

    def __init__(self, entry, idx, branch_prefix):
        super().__init__(entry, branch_prefix)

        # Aliases
        self.producers['idx'] = self.get_idx

        # Constants
        self._idx = idx

    def get_idx(self):
        idx = self.get_from_entry('idx')

        if idx is not VALUE_IS_MISSING:
            return idx

        return self._idx

    def get_from_entry(self, field):
        value = super().get_from_entry(field)

        if value is VALUE_IS_MISSING:
            return VALUE_IS_MISSING

        return value[self._idx]
