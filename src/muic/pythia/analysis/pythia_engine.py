import math
import time
from multiprocessing import Manager, Process
from random import random

import psutil
from ROOT import TFile
from tqdm import tqdm

from muic.core.entries import SimpleProducer, Entry
from muic.core.execution.runtime import get_logger
from muic.pythia.analysis.pythia_input import PythiaInput
from muic.pythia.commons import detector
from muic.pythia.commons.primitives import get_shower_particles, get_muon

base_producers = [
    SimpleProducer('muon', lambda tree: get_muon(tree)),
    SimpleProducer('shower_particles', lambda tree: get_shower_particles(tree))
]


def mt_run(
        inputs, max_entries, worker_count,
        analysis_factory, merge_fn=None,
        event_fraction=1,
        **cmd_args
):
    # Record start time
    start_time = time.perf_counter()

    # Start engine
    with Manager() as manager:
        # Init Locks
        worker_mutex = manager.Lock()
        progress_condition = manager.Condition()

        # Build Context
        worker_ctx = manager.dict()

        # Init Process Info
        gen_sigma = 0
        gen_sigma_error = 0

        worker_ctx['available_sigma'] = 0
        worker_ctx['available_sigma_error'] = 0
        worker_ctx['processed_sigma'] = 0
        worker_ctx['processed_sigma_error'] = 0

        # Loop Inputs
        for input_id, input_args in enumerate(inputs):
            # Get Path
            input_path = input_args[0]

            # Log
            print('Processing {} of {}: {}'.format(input_id + 1, len(inputs), input_path))

            # Open file
            try:
                root_file = TFile.Open(input_path, 'READ')
            except Exception as err:
                print(f"Error opening file {input_path}: {err}")
                continue

            # Short-Circuit: Corrupt file
            if not (root_file and not root_file.IsZombie()):
                print(f"Skipping corrupt or inaccessible file: {input_path}")
                continue

            # Get entry count
            tree = root_file.Get('evt')

            if max_entries > -1:
                total_entries = min(max_entries, tree.GetEntries())
            else:
                total_entries = tree.GetEntries()

            root_file.Close('R')

            # Calculate batch size
            cache_frac = 0.5
            batch_size = max(1000, total_entries // worker_count // 10)
            cache_size_mb = int(psutil.virtual_memory().available / (1024 ** 2) * cache_frac / worker_count)  # in MB

            get_logger().info(f'Setting batch_size to {batch_size} and cache_size to {cache_size_mb} MB')

            # Unpack dataset
            input = PythiaInput(input_id, *input_args)

            # Accumulate XSec
            gen_sigma += input.metadata.sigma * 1e12
            gen_sigma_error += (input.metadata.sigma_error * 1e12) ** 2

            # Init Counters
            worker_ctx['progress'] = 0
            worker_ctx['offset'] = 0

            # Launch Processes
            process_col = list()
            worker_analyzers = manager.list()

            for worker_id in range(worker_count):
                args = (worker_id, input,
                        worker_mutex, progress_condition,
                        worker_ctx, analysis_factory,
                        max_entries, batch_size, cache_size_mb,
                        event_fraction, worker_analyzers)

                process = Process(target=worker_task, args=args)
                process_col.append(process)
                process.start()

            # Monitor progress
            with tqdm(
                    desc='Entries', total=total_entries,
                    unit_scale=True, unit='Entry',
                    colour='#00ff00', mininterval=2
            ) as pbar:
                prev_progress = 0

                while True:
                    # Check workers are alive
                    all_dead = True

                    for process in process_col:
                        process.join(0)

                        if process.is_alive():
                            all_dead = False

                    # Short-Circuit: All workers are dead
                    if all_dead:
                        break

                    # Monitor progress
                    with progress_condition:
                        # Get progress
                        progress = worker_ctx['progress']

                        # Update progress
                        if prev_progress < progress:
                            delta_progress = progress - prev_progress
                            prev_progress = progress
                            pbar.update(delta_progress)

                        # Short-Circuit: Work complete
                        if pbar.n == pbar.total:
                            break

                        # Wait at most 10 seconds for a progress update
                        progress_condition.wait(timeout=10)

            # Join Processes
            for process in process_col:
                process.join()

            # Merge Results
            merge_fn(worker_analyzers)

        # Log
        gen_sigma_error = math.sqrt(gen_sigma_error)
        available_sigma = worker_ctx['available_sigma']
        available_sigma_error = math.sqrt(worker_ctx['available_sigma_error'])
        processed_sigma = worker_ctx['processed_sigma']
        processed_sigma_error = math.sqrt(worker_ctx['processed_sigma_error'])

        print('\n*********************************************************')
        print('Summary')
        print('*********************************************************')
        print('Sigma (Generator) [fb]: ' + '{:.12e}'.format(gen_sigma))
        print('Sigma Error (Generator) [fb]: ' + '{:.12e}'.format(gen_sigma_error))
        print('Sigma (Available) [fb]: ' + '{:.12e}'.format(available_sigma))
        print('Sigma Error (Available) [fb]: ' + '{:.12e}'.format(available_sigma_error))
        print('Sigma (Processed) [fb]: ' + '{:.12e}'.format(processed_sigma))
        print('Sigma Error (Processed) [fb]: ' + '{:.12e}'.format(processed_sigma_error))

    # Log Performance
    total_time = int(time.perf_counter() - start_time)

    print('\n*********************************************************')
    print('Engine Total Time: {} seconds'.format(total_time))
    print('*********************************************************')


def worker_task(
        worker_id, input,
        worker_mutex, progress_condition,
        worker_ctx, analysis_factory,
        max_entries, batch_size, cache_size_mb,
        entry_fraction, worker_analyzers
):
    # Init
    user_producers, analyzers = analysis_factory()

    # Build producer dictionary
    producers = dict()

    for producer in [*base_producers, *user_producers]:
        keys = producer.provides()

        for key in keys:
            producers[key] = producer

    # Open file
    root_file = TFile.Open(input.path, 'READ')
    tree = root_file.Get('evt')

    # Configure cache
    tree.SetCacheSize(cache_size_mb * 1024 * 1024)
    tree.SetCacheLearnEntries(max(1000, batch_size // 100))

    # Calculate max entries
    if max_entries > -1:
        max_entries = min(max_entries, tree.GetEntries())
    else:
        max_entries = tree.GetEntries()

    # Init progress variables
    partial_progress = 0
    update_interval = 2000
    last_update = time.perf_counter()

    # Before run
    for analyzer in analyzers:
        analyzer.before_run(tree)

    # Loop Entries
    keep_alive = True

    while keep_alive:
        # Calculate Ranges
        with worker_mutex:
            start = worker_ctx['offset']
            stop = min(max_entries, start + batch_size)
            worker_ctx['offset'] = stop

        # Exit Condition
        if start >= stop:
            break

        # Set Cache Range
        tree.SetCacheEntryRange(start, stop - 1)

        # Init XSec Accumulator
        batch_available_sigma = 0
        batch_available_sigma_error2 = 0
        batch_processed_sigma = 0
        batch_processed_sigma_error2 = 0

        def post_xsec():
            if batch_available_sigma == 0:
                return

            with progress_condition:
                worker_ctx['available_sigma'] += batch_available_sigma
                worker_ctx['available_sigma_error'] += batch_available_sigma_error2
                worker_ctx['processed_sigma'] += batch_processed_sigma
                worker_ctx['processed_sigma_error'] += batch_processed_sigma_error2

        # Process Batch
        for batch_entry_id in range(stop - start):
            # Calculate next entry id
            entry_id = start + batch_entry_id

            # Short-Circuit: Max event reached
            if max_entries <= entry_id:
                keep_alive = False
                break

            # Randomly choose to process or not
            if not ((entry_fraction == 1) or (random() < entry_fraction)):
                continue

            # Load Event
            try:
                tree.GetEntry(entry_id)

                # Must pass low Q2 cutoff
                if input.q2_low != -1 and tree.rec_Q2 < input.q2_low:
                    continue

                # SHORT-CIRCUIT: MUST PASS UP ENERGY CUTOFF
                if input.q2_up != -1 and input.q2_up <= tree.rec_Q2:
                    continue

                # Calculate Weighted Sigma
                norm_sigma = input.metadata.sigma / input.metadata.weight_sum * 1e12  # mb to fb
                norm_sigma_error = input.metadata.sigma_error / input.metadata.weight_sum * 1e12  # mb to fb
                weighted_sigma = tree.evt_weight * norm_sigma
                weighted_sigma_error = tree.evt_weight * norm_sigma_error

                # Accumulate Available XSec
                batch_available_sigma += weighted_sigma
                batch_available_sigma_error2 += weighted_sigma_error ** 2

                # Short-Circuit: Must pass fiducial cuts
                if not detector.passes_fiducial_cuts(tree):
                    continue

                # Accumulate Processed XSec
                batch_processed_sigma += weighted_sigma
                batch_processed_sigma_error2 += weighted_sigma_error ** 2

                # Analyze entry
                entry = Entry(tree, entry_id, producers)
                entry.input = input
                entry.norm_sigma = norm_sigma
                entry.norm_sigma_error = norm_sigma_error
                entry.weighted_sigma = weighted_sigma
                entry.weighted_sigma_error = weighted_sigma_error

                for analyzer in analyzers:
                    analyzer.process_entry(entry)
            except Exception as err:
                with progress_condition:
                    print('Error occurred on entry_id={}'.format(entry_id))
                    print(err)

                # Post XSec
                post_xsec()

                # Fail gracefully
                return

            # Update partial progress
            partial_progress += 1

            # Short-Circuit: Too early for progress update
            delta_time = (time.perf_counter() - last_update) * 1000

            if delta_time < update_interval:
                continue

            # Publish progress
            last_update = time.perf_counter()
            flush_progress = partial_progress
            partial_progress = 0

            with progress_condition:
                worker_ctx['progress'] += flush_progress
                progress_condition.notify()

        # Post XSec
        post_xsec()

    # Publish Progress
    if partial_progress > 0:
        with progress_condition:
            worker_ctx['progress'] += partial_progress
            progress_condition.notify()

    # After event loop
    for analyzer in analyzers:
        analyzer.after_run()

    # Close root file
    root_file.Close("R")

    # Summary
    with progress_condition:
        print('\n*********************************************************')
        print('Worker %d Summaries' % worker_id)
        print('*********************************************************')

        for clazz, producer in producers.items():
            producer.summary()

    # Register
    with worker_mutex:
        worker_analyzers.append(analyzers)
