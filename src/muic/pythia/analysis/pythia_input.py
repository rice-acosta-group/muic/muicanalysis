from ROOT import TFile


class PythiaInput:

    def __init__(
            self, id, path,
            sigma, sigma_error,
            q2_low, q2_up):
        self.id = id
        self.path = path
        self.sigma = sigma
        self.sigma_error = sigma_error
        self.q2_low = q2_low
        self.q2_up = q2_up
        self.metadata = Metadata(path, sigma, sigma_error)


class Metadata:

    def __init__(self, path, dataset_sigma, dataset_sigma_error):
        # Load Tree
        root_file = TFile.Open(path, 'READ')

        tree = root_file.meta
        tree.GetEntry(0)

        # GET SIM DATA
        self.events_total = tree.events_total
        self.events_processed = tree.events_processed
        self.events_total_weighted = tree.events_total_weighted
        self.events_processed_weighted = tree.events_processed_weighted

        # GET SIGMA
        self.sigma = tree.sigma
        self.sigma_error = tree.sigma_error

        if dataset_sigma is not None and dataset_sigma_error is not None:
            sigma_scale = tree.sigma / dataset_sigma
            self.sigma_error = dataset_sigma_error * sigma_scale

        self.processed_sigma = self.sigma * tree.events_processed_weighted / tree.weight_sum
        self.processed_sigma_error = self.sigma_error * tree.events_processed_weighted / tree.weight_sum

        # GET WEIGHT SUM
        self.weight_sum = tree.weight_sum

        # GET PROCESSES
        self.processes = list()

        for proc_idx in range(tree.vsize_proc):
            self.processes.append(ProcessInfo(tree, proc_idx))

        root_file.Close()

    def print_info(self):
        # Log
        print('\n*********************************************************')
        print('Dataset Metadata')
        print('*********************************************************')
        print('File: %s' % self.input)
        print('Weight Sum (Generator): ' + '{:f}'.format(self.weight_sum))
        print('Weight Sum (Generator-Crosscheck): ' + '{:f}'.format(self.events_total_weighted))
        print('Sigma (Generator) [fb]: ' + '{:.3e}'.format(self.sigma * 1e12))  # mb to fb
        print('Sigma (Generator) Error [fb]: ' + '{:.3e}'.format(self.sigma_error * 1e12))  # mb to fb
        print('Weight Sum (Processed by Pythia): ' + '{:f}'.format(self.events_processed_weighted))
        print('Sigma (Processed by Pythia) [fb]: ' + '{:.3e}'.format(self.processed_sigma * 1e12))  # mb to fb
        print(
            'Sigma Error (Processed by Pythia) [fb]: ' + '{:.3e}'.format(self.processed_sigma_error * 1e12))  # mb to fb


class ProcessInfo:

    def __init__(self, tree, idx):
        self.name = tree.proc[idx]
        self.sigma = tree.proc_sigma[idx]
        self.sigma_error = tree.proc_sigma_error[idx]
        self.processed_sigma = tree.proc_sigma[idx] * tree.events_processed_weighted / tree.weight_sum
        self.processed_sigma_error = tree.proc_sigma_error[idx] * tree.events_processed_weighted / tree.weight_sum
