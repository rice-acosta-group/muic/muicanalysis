machines = {
    'lhmuc': {'name': 'lhmuc', 'label': 'LHmuC', 'Emu': 1500, 'Ep': 7000},
    'muic2': {'name': 'muic2', 'label': 'MuIC2', 'Emu': 1000, 'Ep': 1000},
    'muic': {'name': 'muic', 'label': 'MuIC', 'Emu': 960, 'Ep': 275},
    'hera': {'name': 'hera', 'label': 'HERA', 'Emu': 27.5, 'Ep': 920},
}
