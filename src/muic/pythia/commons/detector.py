import math


def passes_fiducial_cuts(event):
    # SHORT-CIRCUIT: y ACCEPTANCE
    if not (0.01 <= event.y <= 0.9):
        return False

    # SHORT-CIRCUIT: MUON ETA ACCEPTANCE
    if not (abs(event.mu_eta) < 7):
        return False

    # SHORT-CIRCUIT: HADRON ETA ACCEPTANCE
    had_eta = -math.log(math.tan(event.rec_agljb / 2))

    if not (-4 < had_eta < 2.4):
        return False

    # Passed
    return True
