import math

from ROOT import TLorentzVector
from ROOT import TVector3

from muic.core.commons.kinematics import MUON_MASS
from muic.core.commons.lazy import LazyMeta
from muic.core.primitives import Primitive, VALUE_IS_MISSING


class Muon(Primitive, metaclass=LazyMeta):

    def __init__(self, entry, branch_prefix='mu_'):
        super().__init__(entry, branch_prefix=branch_prefix)

        # Constants
        self.mass = MUON_MASS

        # Alias
        self.branch_aliases['energy'] = 'e'

        # Producers
        self.producers['p4'] = self.get_p4
        self.producers['p3'] = self.get_p3
        self.producers['energy'] = self.get_energy
        self.producers['px'] = self.get_px
        self.producers['py'] = self.get_py
        self.producers['pz'] = self.get_pz
        self.producers['pt'] = self.get_pt
        self.producers['p'] = self.get_p
        self.producers['smeared'] = self.get_smeared

    def get_smeared(self):
        return Muon(self.entry, 'mu_sm_')

    def get_p4(self):
        p4 = TLorentzVector()
        p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, MUON_MASS)
        return p4

    def get_p3(self):
        p3 = TVector3()
        p3.SetPtEtaPhi(self.pt, self.eta, self.phi)
        return p3

    def get_pt(self):
        # Try to get from event
        pt = self.get_from_entry('pt')

        if pt is not VALUE_IS_MISSING:
            return pt

        # Try to get from coords
        px = self.get_from_entry('px')
        py = self.get_from_entry('py')

        if (px is not VALUE_IS_MISSING) and (py is not VALUE_IS_MISSING):
            return math.hypot(px, py)

        # Try three vector
        return self.p3.Perp()

    def get_p(self):
        # Try to get from event
        p = self.get_from_entry('p')

        if p is not VALUE_IS_MISSING:
            return p

        # Try to get from coords
        px = self.get_from_entry('px')
        py = self.get_from_entry('py')
        pz = self.get_from_entry('pz')

        if (px is not VALUE_IS_MISSING) and (py is not VALUE_IS_MISSING) and (pz is not VALUE_IS_MISSING):
            return math.hypot(px, py, pz)

        # Try three vector
        return self.p3.Mag()

    def get_px(self):
        # Try to get from event
        px = self.get_from_entry('px')

        if px is not VALUE_IS_MISSING:
            return px

        # Try to get from 4-momentum
        return self.p4.X()

    def get_py(self):
        # Try to get from event
        py = self.get_from_entry('py')

        if py is not VALUE_IS_MISSING:
            return py

        # Try to get from 4-momentum
        return self.p4.Y()

    def get_pz(self):
        # Try to get from event
        pz = self.get_from_entry('pz')

        if pz is not VALUE_IS_MISSING:
            return pz

        # Try to get from 4-momentum
        return self.p4.Z()

    def get_energy(self):
        # Try to get from event
        energy = self.get_from_entry('e')

        if energy is not VALUE_IS_MISSING:
            return energy

        # Try to get from momentum and mass
        return math.hypot(self.p, self.mass)
